<?php

  use App\Middleware\Token;
  use App\Controllers\WebController;

  $app->get('/', WebController::class . ':index')->add(new Token());

?>
