<?php

  namespace App\Middleware;

  // JSON Web Token
  use \Firebase\JWT\JWT;

  class Token{

    public function __invoke($request, $response, $next){
      return $next($request, $response);
    }

  }

?>
